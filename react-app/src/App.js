import './assets/styles/App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/navbar";
import Home from "./views/Home";
import Simulation from "./views/Simulation";

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route index path='/' element={<Home/>} />
        <Route path='/Simulation' element={<Simulation/>} />
      </Routes>
    </Router>
  );
}

export default App;
