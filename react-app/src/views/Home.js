import scene from '../assets/img/scene.png'


import React from 'react';
const Home = () =>{
  return(
    <div className="App">
      <header className="App-header">
        <img src={scene} className="App-logo" alt="logo" />
        <p id="front-title">
          Work on <span style={{whiteSpace: "pre", color: "cyan"}}>Work:<u>                   </u></span><span className="blink">></span><text>&nbsp; &nbsp; A simulation of the impact of technologies on human work</text>
        </p>
      </header>
      <div className="page">
        <div className="questions">
          <div className="question-left">
            🤓 How can we bring political class a clear vision about the automation issue?
          </div>
          <div className="question-right">
          Does technology brings more labour or does it steal it from humans? 😶‍🌫️
          </div>
          <div className="question-left">
            🤔 Is there any way to simulate the effect of technology with other knowledge than economic ?
          </div>
          <div className="question-right">
          What is the impacts of models on society? 🧐
          </div>
          <div className="separation">
            About
          </div>
          <p>
            All those questions are here because the Work On Work projects aims to answer to all of these. WoW is a platform integrating a
          </p>
          <p>
            The platform project seeks to solve a strong need of companies to identify useful digitization of the
            superficial in their investments but also in their civil objectives. It also has as its raison d'être to face
            the huge problem of automation in the labor economy, which currently is mainly a market issue,
            a market obstructed by the modification but especially the reduction of tasks by artificial intelligence technologies.
          </p>
          <p>
            As mentioned in the previous report, there is indeed a risk of job losses of the order of 50% in regions such as
            regions such as Europe or North America, and it is this problem that the platform wishes to address.
            This platform is intended to act as a medium for crystallizing the discussion around automation and its impact on employment, and to
            and its impact on employment, as any technique can do. The Living Lab approach, among others, has an impact on
            The Living Lab approach, among others, has an impact on public policies on employment and in any case allows to increase their investment.
            The approach is therefore highly critical - particularly with regard to entrepreneurial innovation in the Californian or "Silicon Valley" model (fabrics of the economy).
            The approach is therefore highly critical - especially of the Californian or "Silicon Valley" model of entrepreneurial innovation (a network of companies linked to academic technological innovation, whose main aim is to
            is entrepreneurial success) and opts for an approach centered around the human linking the actors of the
            public.
          </p>
          <p>
            The project therefore aims to develop the company by finding alternative methods of financing specific to the
            Living Lab methodology, capable of taking into account the needs of citizens other than by the voice (and not the way)
            monetary.
            Clearly, it is a question of integrating a strong democratic component to the elaboration and the use of the tool as well as the
            project behind it, which is to formulate policies adapted to the redesign of the labor society, for example through
            modes of economic redistribution that are not exclusively wage-earning, i.e. finding solutions at any cost.
          </p>
          <div className="separation">
            Project goals
          </div>
            <p>
            Les objectifs finaux du projet entier (et non du semestre) sont de travailler à :
            <br></br>
            &nbsp; • une plateforme « digital twin » intégrant :
            ◦ Un système multi-agent (SMA) permettant la simulation des déplacements de travaux dus aux technologies
            dites d’intelligence artificielle ;
            ▪ un déclencheur de contact de travailleur précis lorsque les voyants passent au rouge pour l’emploi ou le
            secteur en question ;
            ◦ des données issues de sources économiques récentes mais aussi des travailleurs / employeurs ;
            ◦ un outil économétrique pour les variables de la plateforme ;
            ◦ Un outil de simulation (également SMA) de l’impact d’une technologie numérique sur un poste de travail ;
            Un projet de reprise institutionnelle de la plateforme :
            ◦ en créant un collectif ou en mettant en réseaux les acteurs nécessaires, aboutissant à la création d’un living
            lab (ou autre type de structure) ;
            ◦ l’utilisation principalement par des administrations publiques, entreprises, citoyens, et chercheurs.
          </p>

          <div className="separation">
            Contacts
          </div>
        </div>
    </div>
    </div>
  );
}
export default Home;
