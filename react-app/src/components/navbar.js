import React from 'react';
import {  NavLink } from "react-router-dom";
require("../assets//styles/navbar.css");

const Navbar= () =>{
  return (
  <ul class="topnav">
    <li id="logo">
      <NavLink exact activeClassName="active" to="/">WoW<text id="cyan">.ai</text></NavLink>
    </li>
    <li>
      <NavLink   activeClassName="active" to="/simulation">Simulation</NavLink>
    </li>
  </ul>
  );
}
export default Navbar;
