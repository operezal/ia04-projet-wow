package wow

// AgentI interface will be composed of the standard methods referenced in "TD3 - Prise de decision collective et vote", here "Role" will be the "String" function
type AgentI interface {
	Role() string
	JobSearchPhase(float64)
	TypeWorker() bool
}
