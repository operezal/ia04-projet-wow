package agt

type Technology struct {
	CapFirmID																	int
	Disruptive                                bool
	LabourProductivityOfTechnology            float64
	LabourProductivityNeededToBuildTechnology float64
	ProductionCost														float64
	Price																			float64
	Generation																int
}

func CopyTechnology(tech Technology) *Technology {
	return &Technology{tech.CapFirmID, tech.Disruptive, tech.LabourProductivityOfTechnology, tech.LabourProductivityNeededToBuildTechnology,
	tech.ProductionCost, tech.Price, tech.Generation}
}

func AddTechnology(ID int, disruptive bool, lptech float64, lpnbtech float64, pc float64 , price float64, generation int) *Technology{
	return  &Technology{ID, disruptive, lptech, lpnbtech, pc, price, generation}
}
