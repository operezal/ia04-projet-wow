package agt

import (
	"fmt"
	"ia04_wow/utils"
	"math"
	"sync"
	"time"
)

var countWorker = 0

const timeFactorW = 10

var timeOut = time.Second * 15 / timeFactorW

type Worker struct {
	ID int

	// HARD WRITTEN PARAMETERS
	minimumDesiredWageIncreaseRate float64
	unemploymentBenefit            float64

	age         int
	retiringAge int

	retired          bool
	employed         bool
	epochsUnemployed int
	Skills           float64
	DesiredWage      float64
	Wage             float64
	SavingsOnWage    float64

	EmployedBy int // firmID = 0 if unemployed
	// JOB REQUEST COMMUNICATION
	JobRequestChannels []chan WorkerJobRequest
	EmployerChannel    chan WorkerJobAcceptanceResponse
}

func NewWorker(mwIR, uB, skills float64, age, retireAge int, ret, emp bool) *Worker {
	countWorker++
	return &Worker{
		ID:                             countWorker,
		minimumDesiredWageIncreaseRate: mwIR,
		unemploymentBenefit:            uB,
		age:                            age,
		retiringAge:                    retireAge,
		retired:                        ret,
		employed:                       emp,
		Skills:                         skills,
		DesiredWage:                    0,
		Wage:                           0,
		EmployedBy:                     0,
		JobRequestChannels:             nil,
	}
}

func (w *Worker) Start() {
	// (A/N (@mohamed_eijy): The workers age is going to be in weeks for now as we don't know how granular the temporality is. If retiring age is 60, we input 60*52=3120)
	w.age++

	// If the worker reaches retiring age, he is no longer in the job market
	if w.age >= w.retiringAge {
		w.Retire()
	}
}

func (w *Worker) Role() string {
	//return ""
	panic("implement me")
}

func (w *Worker) ToString() string {
	return fmt.Sprintf("Worker n° %d, skills: %f, requiredWage: %f, savings %f, employedBy %d, ptr %p", w.ID, w.Skills, w.DesiredWage, w.SavingsOnWage, w.EmployedBy, w)
}

func (w *Worker) ToStringShort() string {
	return fmt.Sprintf("[W%d:s%f w:%f]", w.ID, w.Skills, w.DesiredWage)
}

func (w *Worker) ToStringShorter() string {
	return fmt.Sprintf("[W%d]", w.ID)
}

func (w *Worker) SkillsEvolution(SkillAccumulationRateOnTenure float64, SkillDeteriorationRateOnUnemployment float64) {
	var evolutionRate float64
	if w.EmployedBy != 0 { // If employed
		evolutionRate = (1 + SkillAccumulationRateOnTenure)
	} else { // If unemployed
		evolutionRate = (1 - SkillDeteriorationRateOnUnemployment)
	}
	w.Skills = evolutionRate * w.Skills
}

func (w *Worker) Retire() {
	w.retired = true
}

func (w *Worker) WorkerConsumption() float64 {
	var wage float64
	if w.employed {
		wage = w.Wage
	} else {
		wage = w.unemploymentBenefit
	}

	if wage-w.SavingsOnWage <= 0 {
		return 0
	}
	return wage - w.SavingsOnWage
}

func (w Worker) TypeWorker() bool {
	return true
}

func (w *Worker) SetSavingsOnWage(minimumWage float64) {
	//savings = wage*(wage/minimumWage*q1/100) if wage < q5/q1*minimumWage else savings = wage*(wage/minimumWage*q5/100)
	var wage float64
	if w.employed {
		wage = w.Wage
	} else {
		wage = w.unemploymentBenefit
	}
	if wage <= 3*minimumWage {
		w.SavingsOnWage = wage * 6 / 100
	} else {
		w.SavingsOnWage = wage * 18 / 100
	}
}

//**** JOB SEARCH PHASE ****//

func (w *Worker) JobSearchPhase(minWage float64) {
	// For the worker, they start by generating a desired wage, then send a job request to all the firm channels, wait for an answer, and finally accept the highest wage offer

	// Generating a desired wage for the set epoch
	w.SetDesiredWage(minWage)

	// Create a channel through which we will receive offers
	workOfferChan := make(chan FirmJobRequest, len(w.JobRequestChannels))
	// Store responses in a slice
	var jobOffers []FirmJobRequest

	// 	Create a job request
	jobRequest := WorkerJobRequest{
		workerID:        w.ID,
		desiredWage:     w.DesiredWage,
		workerOfferChan: workOfferChan,
	}

	//	Send the request to each firm and wait for an answer we'll put in a slice/array
	var wg sync.WaitGroup
	for _, ch := range w.JobRequestChannels {
		wg.Add(1)
		go func(InChannel chan WorkerJobRequest) {
			defer wg.Done()
			InChannel <- jobRequest
		}(ch)
	}
	wg.Wait()

	jobOffers = make([]FirmJobRequest, 0, len(w.JobRequestChannels))
WaitingForOffers:
	for {
		select {
		case workOffer := <-workOfferChan:
			if workOffer.offeredWage > 0 {
				fmt.Println("Worker", w.ToStringShorter(), "received another offer: Company", workOffer.firmID, "offered", workOffer.offeredWage, "$")
				jobOffers = append(jobOffers, workOffer)
			}
			// else {
			// 	fmt.Println("Worker", w.ToStringShorter(), "got rejected by Company", workOffer.firmID)
			// }
		case <-time.After(timeOut):
			fmt.Println("Worker", w.ToStringShorter(), "stopped waiting for offers by its", cap(w.JobRequestChannels), "selected companies ")
			//close(workOfferChan)
			break WaitingForOffers
			//default:
		}
	}

	if len(jobOffers) > 0 {
		// Go through our job offers, pick the one with the highest wage, and send a response to accept, refuse everything else
		chosenJobIdx := utils.OptimumIndex(jobOffers, func(jobOffer1 FirmJobRequest, jobOffer2 FirmJobRequest) bool {
			return (jobOffer1.offeredWage > jobOffer2.offeredWage) // ERASE
		})
		// && jobOffer1.offeredWage > w.Wage && jobOffer1.offeredWage > w.unemploymentBenefit

		// If the offered wage for all job offers is 0, that means no firm has offered us a job, else we accept the best offer, firms do not except us to respond if they deny us a job offer
		if jobOffers[chosenJobIdx].offeredWage != 0 {
			var wg2 sync.WaitGroup
			for i, jobOffer := range jobOffers {
				if jobOffer.offeredWage != 0 {
					accepted := i == chosenJobIdx
					betterOffer := jobOffer.offeredWage > w.Wage && jobOffer.offeredWage > w.unemploymentBenefit
					if accepted == true {
						if betterOffer == true {
							if w.EmployedBy != 0 {
								// Quit current company
								fmt.Println("Worker", w.ToStringShorter(), "quits Company", w.EmployedBy)
								fmt.Println(w.EmployerChannel)
								w.EmployerChannel <- WorkerJobAcceptanceResponse{
									worker:     w,
									acceptance: false,
									quits:      true,
								}

							}
							w.JobFound(jobOffers[chosenJobIdx].firmID, jobOffers[chosenJobIdx].offeredWage, jobOffers[chosenJobIdx].firmOfferRChan)

							fmt.Println("Worker", w.ToStringShorter(), "accepts Company", jobOffer.firmID)
							wg2.Add(1)
							go func(InChannel chan WorkerJobAcceptanceResponse) {
								defer wg2.Done()
								InChannel <- WorkerJobAcceptanceResponse{
									worker:     w,
									acceptance: true,
									quits:      false,
								}
							}(jobOffer.firmOfferRChan)

						} else {
							//fmt.Println("Worker", w.ToStringShorter(), "refuses Company", jobOffer.firmID)
							wg2.Add(1)
							go func(InChannel chan WorkerJobAcceptanceResponse) {
								defer wg2.Done()
								InChannel <- WorkerJobAcceptanceResponse{
									worker:     w,
									acceptance: false,
									quits:      false,
								}
							}(jobOffer.firmOfferRChan)
						}
					} else {
						//fmt.Println("Worker", w.ToStringShorter(), "refuses Company", jobOffer.firmID)
						wg2.Add(1)
						go func(InChannel chan WorkerJobAcceptanceResponse) {
							defer wg2.Done()
							InChannel <- WorkerJobAcceptanceResponse{
								worker:     w,
								acceptance: false,
								quits:      false,
							}
						}(jobOffer.firmOfferRChan)
					}
				}
			}
			wg2.Wait()
		}

	} else {
		//fmt.Println("Worker", w.ToStringShorter(), "received no job offers")
		//	if we were unemployed, we stay unemployed
		if !w.employed {
			w.epochsUnemployed = w.epochsUnemployed + 1
		}
	}
}

func (w Worker) IsEmployed() bool {
	return w.employed
}

func (w *Worker) JobFound(firmId int, newWage float64, chanJob chan WorkerJobAcceptanceResponse) {
	//fmt.Println("Worker", w.ToStringShorter(), "has been affected the firm of ID:", firmId)
	// Change our employment status
	w.employed = true
	w.epochsUnemployed = 0

	// Register our new employer
	w.EmployedBy = firmId
	w.EmployerChannel = chanJob
	// Register our new wage
	w.Wage = newWage
}

func (w *Worker) EmptyPreviousChannels(size int) {
	//fmt.Println(w.ToStringShorter(), "empty")
	w.JobRequestChannels = make([]chan WorkerJobRequest, 0, size)
}

func (w *Worker) SetFirmChannels(channels []chan WorkerJobRequest) {
	w.EmptyPreviousChannels(len(channels))
	w.JobRequestChannels = append(w.JobRequestChannels, channels...)
}

func (w *Worker) SetDesiredWage(minWage float64) {
	fmt.Println("WAGES", w.unemploymentBenefit, w.Wage, minWage)
	if !w.employed {
		// if the worker is unemployed, they send the value set in wage (which should correspond to the minimum unemployment benefit set in the world in the previous epoch
		w.DesiredWage = math.Max(w.unemploymentBenefit, minWage) // comes from government
	} else {
		// if employed, they send their wage + a premium determined by a scalar epsilon
		evolutionWage := w.Wage * (1 + w.minimumDesiredWageIncreaseRate)
		w.DesiredWage = math.Max(math.Max(w.unemploymentBenefit, minWage), evolutionWage)
	}
}

func (w *Worker) SetUnemploymentBenefit(uB float64) {
	w.unemploymentBenefit = uB
}
