package agt

import (
	"fmt"
)

type Government struct {
	unemploymentBenefit float64
	minimumWage         float64
	taxRate             float64
}

func NewGovernment(initialUnemploymentBenefit float64, initialMinimumWage float64, initialTaxRate float64) *Government {
	return &Government{
		unemploymentBenefit: initialUnemploymentBenefit,
		minimumWage:         initialMinimumWage,
		taxRate:             initialTaxRate,
	}
}

func (g *Government) Update(fawub, appt float64, averageWage, aggregateProductivity []float64) { // to be used after the calculation of average wage in WoW
	g.updateUnemploymentBenefit(fawub, averageWage)
	g.updateMinimumWage(appt, aggregateProductivity)
}

func (g *Government) Role() string {
	//return ""
	panic("implement me")
}

func (g *Government) ToString() string {
	return fmt.Sprintf("[Government] unemployment benefit: %f, minimum wage: %f, tax rate: %f", g.unemploymentBenefit, g.minimumWage, g.taxRate)
}

func (g *Government) ToStringShort() string {
	return fmt.Sprintf("[Gov] UB: %f, MW: %f, TR: %f]", g.unemploymentBenefit, g.minimumWage, g.taxRate)
}

func (g *Government) GetUnemploymentBenefit() float64 {
	return g.unemploymentBenefit
}

func (g *Government) updateUnemploymentBenefit(fractionOfAverageWageForUnemploymentBenefit float64, averageWage []float64) {
	if len(averageWage) > 0 {
		fmt.Println(g.ToStringShort(), "Updating unemployment benefit", fractionOfAverageWageForUnemploymentBenefit, averageWage, averageWage[len(averageWage)-1])
		g.unemploymentBenefit = fractionOfAverageWageForUnemploymentBenefit * averageWage[len(averageWage)-1]
		fmt.Println(g.ToStringShort(), "Updating unemployment benefit")
	}
}

func (g *Government) updateMinimumWage(aggregateProductivityPassThrough float64, aggregateProductivity []float64) {
	if len(aggregateProductivity) > 1 {
		g.minimumWage = g.minimumWage * (1 + aggregateProductivityPassThrough*(aggregateProductivity[len(aggregateProductivity)-1]-aggregateProductivity[len(aggregateProductivity)-2])/aggregateProductivity[len(aggregateProductivity)-2])
		fmt.Print(aggregateProductivity)
	}
}

func (g Government) GetMinimumWage() float64 {
	return g.minimumWage
}
