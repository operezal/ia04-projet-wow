# IA04 - Projet WorkonWork 💼💵

**Mohamed Fall, Omar Pérez, Antoine Kryus, et Xingyu Du**

**Équipe "4 continents 🌍"**

[Dépôt gitlab](https://gitlab.utc.fr/operezal/ia04-projet-wow)


## I) Fonctionnement du projet

Notre projet "WorkOnWork" s'insère dans l'ambition de faire une recherche extensive sur le problème de l’emploi et de l’automatisation (PR associée), et d'effectuer une analyse critique de la relation qu'entretient le marché de l'emploi avec l'innovation technologique. Pour aller dans ce sens, l'objectif de ce projet est d'établir une simulation du marché de l'emploi permettant d'orchestrer le phénomène d'évolution technologique et ses répercussions sur la productivité du capital et la masse salariale, en se basant sur un article scientifique d'un SMA évolutionnaire.

Pour ce faire, notre simulation a pour vocation de répliquer des phénomènes économiques clés du marché de l'emploi et de la production, en y injectant le concept de génération d'outils technologiques. Elle coordonne donc séquentiellement 7 étapes principales qui se répètent en cycle (périodes) qui sont listées ci-dessous.

#### Agents

Les agents économiques présents dans notre simulation sont:
* les **firmes technologiques**; font de la R&D et produisent les technologies permettant d'améliorer la productivité des firmes de consommation,
* les **firmes de consommation**,
* les **travailleurs**,
* le **gouvernement**; s'occupe pour l'instant d'ajuster et de fournir les aides au chômage.

#### Construction du modèle

Bien sûr le modèle établi est un modèle simplifié de l'article lui-même, et ne prend pas en compte un certains nombre d'évènnements macroéconomiques d'ampleur comme par exemple la compétition inter-industrielle (agents industrie) et l'investissement par le secteur financier (agents banque).

Une grande importance a été attachée à voir la construction du modèle sur le temps long. Par exemple, le gouvernement et les technologies ont été considérés en tant qu’“agents” dans nos discussions alors qu’ils sont en réalité de grandes structures aidées par les agents eux-même dont le coordinateur, pour permettre à la suite de leur attribuer des propriété ou de les faire communiquer.

#### Étapes par cycle

Les 7 étapes d'un cycle de la simulation:
1. Les firmes technologiques délivrent des outils aux firmes de consommations (reservées dans leur carnet de commandes) de la période précédente.
2. Les firmes technologiques conduisent de la R&D et créent des technologies de paradigme nouveau, ou des améliorations incrémentales de générations précédentes, ou des technologies imitées sur leurs voisins. Elles signalent leur production aux firmes de consommation.
3. Les firmes de consommation choississent les technologies à commander pour le prochain cycle.
4. Selon leurs projections de production, les firmes ouvrent les canditatures à leurs postes. Les travailleurs (salariés ou au chômage), postulent à un sous-ensemble de firmes et acceptent la meilleure offre qui leur est faite.
5. Les firmes paient les salaires et les bonus. Le gouvernement paie les aides au chômage.
6. Les travailleurs consomment les produits vendus par les firmes de consommation avec une partie de leur salaire.
7. Les firmes déterminent leurs projections de production pour le prochain cycle.

##### Exemple des étapes 4 et 5 de l'étape du marché du travail

Le processus de recherche d'emploi se fait par étapes, mais tous les agents se lancent dans cette phase en parallèle. La première phase consiste en l’envoi de candidatures : les`Workers` postulent à un ensemble d’entreprises (`Firm`) déterminées aléatoirement. La probabilité de postuler à une entreprise étant définie selon le modèle comme proportionnelle à la part de marché détenue par l’entreprise.
Deuxièmement, les entreprises feront réception des candidatures jusqu’à un certain délai, si elles requièrent des employés, pour ensuite faire la sélection des meilleurs candidats. Le critère de sélection étant le salaire requis par le `Worker` et le nombre de `Workers` requis par l’entreprise.
Les `Workers`, attendant depuis l’envoi de leur candidature, reçoivent alors des réponses au fur et à mesure des entreprises. Après un certain délai, les `Workers` décident parmi les propositions reçues et leur emploi actuel (s’ils sont employés), laquelle est la meilleure selon leur "utilité" qui est le salaire proposé par les entreprises ayant accepté la candidature. Si une proposition est meilleure que l’emploi actuel d’un Worker, il démissionne de l’entreprise.
Finalement, le `Worker` envoie sa réponse (positive ou négative) et si la réponse est positive, il est ajouté à la `Workforce` de l’entreprise et commence à percevoir un salaire depuis l’itération actuelle.
Ensuite, la phase de paiement des salaires s'effectue, chaque `Worker` verra modifié son salaire et il s'en servira pour la phase de consommation.

## II) Installation du projet, lancement et architecture

#### Instructions d'installation sur Linux

Pour React:
- Télécharger et installer [Node.Js](https://nodejs.org/en/);
- Télécharger et installer [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm);
- Lancer dans le  dossier `react-app` du projet pour installer React et toutes les dépendances :

```
npm i
```

Pour Go:
- Télécharger et installer la dernière version de [Go](https://go.dev/doc/install) ;

#### Instructions  de lancement

Sans ordre:
- Lancer l'interface React dans le dossier `react-app` :
```
npm start
```
- Lancer l'application go (serveur et simulation) dans le dossier `sma` :
```
go run ./cmd/launch_app.go
```
Au premier lancement, Go téléchargera automatiquement les dépendances.

#### Architecture

##### Architecture technique

Le projet est composé d'une application web **React**, déployant l'ensemble des éléments graphiques et permettant de lancer la simulation. Le programme en backend est un serveur en **Go** faisant tourner le code de notre simulation.

##### Fonctionnalités

Le serveur délivre une API RESTful à travers laquelle le client peut accéder à la ressource `Report` à travers la *route* `epoch_done` qui est un ensemble de variables aggrégeant certaines données simulées (exemple: le chômage), d'intérêt pour voir la cohérence du modèle. L'application React récupère ces données et les traites pour les ajouter à des listes d'éléments composant des graphique de la librairie **Plotly**, et met à jour ces graphiques en temps réel avec les données récupérées toutes les secondes, mises à jour uniquement si l'époque est différente de celle actuelle. Il y a aussi d'autres *routes*, présentes pour recevoir les requêtes React des boutons **Start** et **Stop**, qui permettent respectivement de lancer la simulation (en faisant un **Reset** du système multi-agent s'il a déjà été lancé et arrêté par le bouton *Stop*) et de désactiver le modèle (qui s'arrêtera à la fin du cycle en cours). L'architecture est donc toute prêt pour incorporer dans le futur un changement intéractif des nombreux paramètres du modèle.

##### Architecture du code

Le système dans le modèle Go est centré autour du struct `WorkOnWork`, qui agit comme un coordinateur du modèle et donc est composé de tous nos agents, d'une grande structure de paramètres, ainsi que des différents calculs responsables de la suite d'évenements (étapes) voulus dans notre simulation. Pendant le lancement de la simulation, un nouvel objet `WorkOnWork` est créé, et les agents et paramètres sont fournis avant de lancer la fonction `Simulate`, dont le rôle et d'effectuer une initialisation de la simulation, puis de l'éxecuter pendant un nombre de cycles déterminé.
Un cycle (`epoch` dans le code) de simulation est censé suivre la timeline décrite plus haut en partie 1.
Au bout de chaque cycle, un struct sérialisable `WorldState` est généré et reporté dans l'attribut `Report` de l'object `WorkOnWork`, contenant l'ensemble des variables analytiquement pertinentes. Celui-ci peut alors être envoyé en réponse à la requête executée périodiquement par notre client React (fonction `fetch`).

Un cycle est divisé en fonctions le long des étapes de la timeline car les agents doivent être coordonnées pour suivre un cheminement structuré. Chaque fonction va une a une lancer ou utiliser les agents concernés par la phase macroéconomique correspondante.

##### Communication inter-agents

Pour la phase de recherche d'emploi (ainsi que pour la commande et livraison des technologies entre firmes) les agents communiquent via des channels. Pour chaque étape, un channel de type différent est utilisé, car par exemple pour la recherche d'emploi, la nature des messages est différente:
-  pour la première étape un `Worker` communique à une entreprise son salaire souhaité mais;
-  pour la deuxième étape, l'entreprise communiquera si elle retient le candidat ou pas et quel est le salaire qu'elle lui propose;
-  pour la troisième étape le candidat notifiera sa décision via un booléen aux entreprises l'ayant accepté et à son entreprise actuelle s'il décide de la quitter.

Néanmoins, pour ces étapes, le principe de communication reste le même: un agent envoie un message à un autre agent via le channel qu'on lui fournit et dans son message, il envoie le channel de réponse sur lequelle il sera à l'écoute jusqu'à un certain délai. Nous pouvons voir ce délai comme la "patience" de l'agent à calibrer évidemment selon le temps de calcul pris par l'ordinateur qui produit la simulation.

## III) Points positifs et négatifs du projet, analyse de variation des métaparamètres

##### Atout et intérêt

Les points positifs du projet sont qu'on a réussi à établir une intéraction entre les agents qui veillent à maximiser chacun leur profit (une utilité qui leur est propre en fonction de leur situation) tout en comptant d'une "patience" qui est traduite dans le modèle par des *time-outs*. La valeur ajoutée peut-être bénéfique pour ce domaine si l'accès en open-source au développement de l'application serait permis. Les connaissances plus détaillées acquises sur de l'économie de l'innovation (modèles évolutionnaires), et de l'économie du travail, est également un grand atout.

##### Étape de calibration

Une grande structure dans `parameters.go` nous de voir l'ensemble des paramètres du modèle, ainsi que les données d'initialisation de certains agents, ceci à des fins de practicité pour la calibration des résultats, même si les formules des fonctions à modifier restent à chercher au debuggage. 
Néanmoins, la calibration du modèle rélève un des plus grands défis auxquels on a dû faire face lors de ce projet. En effet, il fallait calibrer les temps pour les différentes séquences d'intéraction entre les agents et les paramètres économiques décrivant le monde simulé, car les formules d'estimation de salaires, production, consommation et d'embauche en dépendent. Il en résulte, actuellement, un modèle partiellement fini puisque, certes la quasi-totalité des fonctions souhaitées ont été codées, elles n'ont cependant pas été encore bien calibrées. La validation, par exemple celle de Monte Carlo par répétitions, n'a par ailleurs pas été implémentée et aurait été nécessaire, une fois le modèle calibré, à voir si cette calibration sera fructueuse.

##### Viabilité du projet pour IA04

Le projet a été également trop ambitieux pour être réalisé en 2 mois (codé en un peu plus d'un mois, premier push le 2 décembre, le restant étant consacré à la modélisation à 4 sans compter la PR), ce qui est très dommage car nous aurions voulu avoir plus de résulats et construire une interface graphique repésentant les interactions entre groupes d’agents (les chercheurs étant arrivés au modèle de l’article travaillant à faire évoluer, faire marcher et calibrer les systèmes multi-agents évolutionnaires depuis 40 ans). Le modèle a été difficile à implémenter, en raison de sa taille (nombre et complexité des fonctions) et des nombreuses phases qu’il comptait simuler.

##### Résultats

Pour ce qui est de l'analyse des paramètres et métaparamètres, il nous faudrait tester plus de configurations. Nos premiers résultats ou les salaires de nos travailleurs été exponentiels après environ 30 cycles nous montrent que la calibration est d'importance capitale comme nous le savions déjà, car dans le monde réel, effectivement certains ont des salaires exponentiels, mais il n'en résulte pas la même chose pour tout le monde. Les résultats du chômage, par exemple sont compris au regards des fonctions mais notamment parce que les calculs sur les variables qui se répecutent sur le chômage étaient parfois simpifiés (exemple de la *RequiredWorkforce*).

## IV) Annexes

#### 0) Lien du dépôt gitlab

[Lien vers le git](https://gitlab.utc.fr/operezal/ia04-projet-wow)

#### 1) Schéma de compréhension de l'intéraction des agents.
![](https://md.picasoft.net/uploads/upload_b2557b4849f2730e944b3fc37924b2b9.png)

#### 2) Extrait du document *de travail* sur les fonctions, agents, et paramètres du modèle pour concevoir l'UML des agents
[Lien vers le document de travail](https://wwwetu.utc.fr/~kryusant/Model_info.pdf)

#### 3) URL de l'article sur lequel le modèle s'inspire dans la PR liée au projet IA04
[G. Dosi , M.C. Pereira , A. Roventini , M.E. Virgillito, "Technological paradigms, labour creation and destruction in a multi-sector agent-based model", Research Policy, 51 (2022)](https://www.sciencedirect.com/science/article/abs/pii/S0048733322000889?via%3Dihub)

#### 4) Diagrammes de séquence pour le marché du travail et le marché des technologies
![](https://md.picasoft.net/uploads/upload_e79b5c5d1bd408ad785fa2a8a87c5170.png)
